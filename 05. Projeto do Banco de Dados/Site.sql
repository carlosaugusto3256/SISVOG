-- -----------------------------------------------------
-- Table Mensagem
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Mensagem (
  id INT UNSIGNED NOT NULL,
  nome VARCHAR(70) NOT NULL,
  email VARCHAR(60) NOT NULL,
  assunto VARCHAR(150) NOT NULL,
  mensagem MEDIUMTEXT NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Imagem
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Imagem (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  caminho VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Administrador
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Administrador (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(70) NOT NULL,
  usuario VARCHAR(40) NOT NULL,
  email VARCHAR(60) NOT NULL,
  senha VARCHAR(64) NOT NULL,
  Imagem_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX usuario_UNIQUE (usuario ASC),
  UNIQUE INDEX email_UNIQUE (email ASC),
  INDEX fk_Administrador_Imagem1_idx (Imagem_id ASC),
  CONSTRAINT fk_Administrador_Imagem1
    FOREIGN KEY (Imagem_id)
    REFERENCES Imagem (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Slide
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Slide (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  link VARCHAR(200) NOT NULL,
  tempo INT(1) UNSIGNED NOT NULL,
  Imagem_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Slide_Imagem1_idx (Imagem_id ASC),
  CONSTRAINT fk_Slide_Imagem1
    FOREIGN KEY (Imagem_id)
    REFERENCES Imagem (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Loja
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Loja (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50) NOT NULL,
  status SET('ativa', 'espera', 'remocao') NOT NULL,
  data_remocao DATETIME NULL,
  facebook VARCHAR(100) NULL,
  youtube VARCHAR(100) NULL,
  obsevacao VARCHAR(45) NULL,
  logradouro VARCHAR(100) NULL,
  bairro VARCHAR(60) NULL,
  cidade VARCHAR(60) NULL,
  estado VARCHAR(60) NULL,
  cep VARCHAR(10) NULL,
  tel1 VARCHAR(15) NULL,
  tel2 VARCHAR(15) NULL,
  email VARCHAR(60) NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Noticia
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Noticia (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(200) NOT NULL,
  resumo TINYTEXT NOT NULL,
  texto LONGTEXT NOT NULL,
  visibilidade TINYINT NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Foto_Noticia
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Foto_Noticia (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  caminho VARCHAR(45) NOT NULL,
  Noticia_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Foto_Noticia_Noticia1_idx (Noticia_id ASC),
  CONSTRAINT fk_Foto_Noticia_Noticia1
    FOREIGN KEY (Noticia_id)
    REFERENCES Noticia (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Resposta
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Resposta (
  Mensagem_id INT UNSIGNED NOT NULL,
  Administrador_id INT UNSIGNED NOT NULL,
  resposta MEDIUMTEXT NOT NULL,
  assunto VARCHAR(150) NOT NULL,
  PRIMARY KEY (Mensagem_id, Administrador_id),
  INDEX fk_Mensagem_has_Administrador_Administrador1_idx (Administrador_id ASC),
  INDEX fk_Mensagem_has_Administrador_Mensagem1_idx (Mensagem_id ASC),
  CONSTRAINT fk_Mensagem_has_Administrador_Mensagem1
    FOREIGN KEY (Mensagem_id)
    REFERENCES Mensagem (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Mensagem_has_Administrador_Administrador1
    FOREIGN KEY (Administrador_id)
    REFERENCES Administrador (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Modificacao
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Modificacao (
  Administrador_id INT UNSIGNED NOT NULL,
  Noticia_id INT UNSIGNED NOT NULL,
  data_hora DATETIME NOT NULL,
  PRIMARY KEY (Administrador_id, Noticia_id),
  INDEX fk_Administrador_has_Noticia_Noticia1_idx (Noticia_id ASC),
  INDEX fk_Administrador_has_Noticia_Administrador1_idx (Administrador_id ASC),
  CONSTRAINT fk_Administrador_has_Noticia_Administrador1
    FOREIGN KEY (Administrador_id)
    REFERENCES Administrador (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Administrador_has_Noticia_Noticia1
    FOREIGN KEY (Noticia_id)
    REFERENCES Noticia (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Cliente
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Cliente (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  cpf VARCHAR(15) NOT NULL,
  nome VARCHAR(70) NOT NULL,
  email VARCHAR(60) NOT NULL,
  senha VARCHAR(64) NOT NULL,
  Imagem_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Cliente_Imagem1_idx (Imagem_id ASC),
  UNIQUE INDEX cpf_UNIQUE (cpf ASC),
  CONSTRAINT fk_Cliente_Imagem1
    FOREIGN KEY (Imagem_id)
    REFERENCES Imagem (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Endereco
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Endereco (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  logradouro VARCHAR(100) NOT NULL,
  bairro VARCHAR(60) NOT NULL,
  cidade VARCHAR(60) NOT NULL,
  estado VARCHAR(60) NOT NULL,
  cep VARCHAR(10) NOT NULL,
  tel1 VARCHAR(15) NOT NULL,
  tel2 VARCHAR(15) NULL,
  email VARCHAR(60) NOT NULL,
  Cliente_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Endereco_Cliente1_idx (Cliente_id ASC),
  CONSTRAINT fk_Endereco_Cliente1
    FOREIGN KEY (Cliente_id)
    REFERENCES Cliente (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Status
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Status (
  id INT NOT NULL,
  tipo SET('processando', 'pagConfirmado', 'enviado', 'recebido') NOT NULL,
  data_hora_feito DATETIME NOT NULL,
  data_hora_sistema DATETIME NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Pedido
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Pedido (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  memorando TINYTEXT NULL,
  rastreio VARCHAR(20) NULL,
  total DECIMAL UNSIGNED NOT NULL,
  Cliente_id INT UNSIGNED NOT NULL,
  Status_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Pedido_Cliente1_idx (Cliente_id ASC),
  INDEX fk_Pedido_Status1_idx (Status_id ASC),
  CONSTRAINT fk_Pedido_Cliente1
    FOREIGN KEY (Cliente_id)
    REFERENCES Cliente (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Pedido_Status1
    FOREIGN KEY (Status_id)
    REFERENCES Status (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Email
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Email (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  assunto VARCHAR(150) NOT NULL,
  mensagem MEDIUMTEXT NOT NULL,
  Pedido_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Email_Pedido1_idx (Pedido_id ASC),
  CONSTRAINT fk_Email_Pedido1
    FOREIGN KEY (Pedido_id)
    REFERENCES Pedido (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Categoria
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Categoria (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Produto
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Produto (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  visibilidade TINYINT NOT NULL,
  nome VARCHAR(70) NOT NULL,
  quantidade INT UNSIGNED NOT NULL,
  preco DECIMAL UNSIGNED NOT NULL,
  acessos INT UNSIGNED NOT NULL,
  descricao MEDIUMTEXT NOT NULL,
  Categoria_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Produto_Categoria1_idx (Categoria_id ASC),
  CONSTRAINT fk_Produto_Categoria1
    FOREIGN KEY (Categoria_id)
    REFERENCES Categoria (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Lista_Desejos
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Lista_Desejos (
  Cliente_id INT UNSIGNED NOT NULL,
  Produto_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (Cliente_id, Produto_id),
  INDEX fk_Lista_Desejos_Produto1_idx (Produto_id ASC),
  CONSTRAINT fk_Lista_Desejos_Cliente1
    FOREIGN KEY (Cliente_id)
    REFERENCES Cliente (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Lista_Desejos_Produto1
    FOREIGN KEY (Produto_id)
    REFERENCES Produto (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Foto_Produto
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Foto_Produto (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  caminho VARCHAR(45) NOT NULL,
  Produto_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Foto_Produto_Produto1_idx (Produto_id ASC),
  CONSTRAINT fk_Foto_Produto_Produto1
    FOREIGN KEY (Produto_id)
    REFERENCES Produto (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Pedido_has_Produto
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Pedido_has_Produto (
  Pedido_id INT UNSIGNED NOT NULL,
  Produto_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (Pedido_id, Produto_id),
  INDEX fk_Pedido_has_Produto_Produto1_idx (Produto_id ASC),
  INDEX fk_Pedido_has_Produto_Pedido1_idx (Pedido_id ASC),
  CONSTRAINT fk_Pedido_has_Produto_Pedido1
    FOREIGN KEY (Pedido_id)
    REFERENCES Pedido (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Pedido_has_Produto_Produto1
    FOREIGN KEY (Produto_id)
    REFERENCES Produto (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
